const { hashSync } = require('bcrypt');

class User {
  constructor({ id, name, cpf, email, age, password }) {
    this.id = id;
    this.name = name;
    this.cpf = cpf;
    this.email = email;
    this.age = age;
    this.password = hashSync(password, 10);
  }
}

module.exports = User;
