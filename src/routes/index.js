const { router, get, post, put, del } = require('microrouter');
const UserController = require('../controllers/UserController');

const userController = new UserController();

const listUsersByAge = router(
  get('/user', async (request, response) =>
    userController.index(request, response),
  ),
);

const listUserById = router(
  get('/user/:id', async (request, response) =>
    userController.show(request, response),
  ),
);

const createUser = router(
  post('/user', async (request, response) =>
    userController.create(request, response),
  ),
);

const updateUser = router(
  put('/:id', async (request, response) =>
    userController.update(request, response),
  ),
);

const deleteUser = router(
  del('/:id', async (request, response) =>
    userController.delete(request, response),
  ),
);

module.exports = {
  listUsersByAge,
  listUserById,
  createUser,
  updateUser,
  deleteUser,
};
