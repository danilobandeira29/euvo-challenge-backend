const { send, json } = require('micro');
const UsersRepository = require('../repositories/UsersRepository');
const CreateUserService = require('../services/CreateUserService');
const UpdateUserService = require('../services/UpdateUserService');
const ListUsersByAgeService = require('../services/ListUsersByAgeService');
const DeleteUserService = require('../services/DeleteUserService');
const ListUserByIdService = require('../services/ListUserByIdService');

const usersRepository = new UsersRepository();

class UserController {
  async index(request, response) {
    try {
      const { age } = request.query;

      const listUsersByAgeService = new ListUsersByAgeService(usersRepository);

      const users = await listUsersByAgeService.execute(age);

      send(response, 200, users);
    } catch (err) {
      send(response, 400, { status: 'Error', message: err.message });
    }
  }

  async show(request, response) {
    try {
      const { id } = request.params;

      const listUserByIdService = new ListUserByIdService(usersRepository);

      const user = await listUserByIdService.execute(Number(id));

      send(response, 200, user);
    } catch (err) {
      send(response, 404, { status: 'Error', message: err.message });
    }
  }

  async create(request, response) {
    try {
      const { name, email, cpf, password, age } = await json(request);

      const createUserService = new CreateUserService(usersRepository);

      const user = await createUserService.execute({
        name,
        email,
        cpf,
        password,
        age,
      });

      send(response, 200, user);
    } catch (err) {
      send(response, 400, { status: 'Error', message: err.message });
    }
  }

  async update(request, response) {
    try {
      const { id } = request.params;

      const { name, email, password, cpf, age } = await json(request);

      const updateUserService = new UpdateUserService(usersRepository);

      await updateUserService.execute({
        name,
        email,
        cpf,
        password,
        age,
        id,
      });

      send(response, 204);
    } catch (err) {
      send(response, 400, { status: 'Error', message: err.message });
    }
  }

  async delete(request, response) {
    try {
      const { id } = request.params;

      const deleteUserService = new DeleteUserService(usersRepository);

      await deleteUserService.execute(Number(id));

      send(response, 204);
    } catch (err) {
      send(response, 400, { status: 'Error', message: err.message });
    }
  }
}

module.exports = UserController;
