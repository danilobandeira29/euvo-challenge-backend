exports.up = function (knex) {
  return knex.schema.createTable('User', function (table) {
    table.increments('id').primary();
    table.string('name').notNullable();
    table.string('email').notNullable();
    table.string('password').notNullable();
    table.string('cpf').notNullable();
    table.integer('age').notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('User');
};
