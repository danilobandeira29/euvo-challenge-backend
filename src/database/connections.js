const knex = require('knex');
const config = require('../../knexfile');

class Connection {
  constructor() {
    this.knexRepository = knex(config);
  }
}

module.exports = Connection;
