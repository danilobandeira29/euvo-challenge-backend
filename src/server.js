const { router, get, post, put, del } = require('microrouter');
const {
  listUsersByAge,
  listUserById,
  createUser,
  updateUser,
  deleteUser,
} = require('./routes');

module.exports = router(
  get('/user', listUsersByAge),
  get('/user/:id', listUserById),
  post('/user', createUser),
  put('/:id', updateUser),
  del('/:id', deleteUser),
);
