const { hashSync } = require('bcrypt');

class CreateUserService {
  constructor(userRepository) {
    this.usersRepository = userRepository;
  }

  async execute({ name, email, cpf, password, age }) {
    const findUserWithSameEmail = await this.usersRepository.findByEmail(email);

    if (findUserWithSameEmail.length !== 0) {
      throw new Error('This E-mail is already used');
    }

    const passwordCrypted = hashSync(password, 10);

    const userId = await this.usersRepository.create({
      name,
      email,
      cpf,
      password: passwordCrypted,
      age,
    });

    return { id: userId, name, email, cpf, age };
  }
}

module.exports = CreateUserService;
