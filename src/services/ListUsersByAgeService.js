class ListUsersByAgeService {
  constructor(usersRepository) {
    this.usersRepository = usersRepository;
  }

  async execute(age) {
    const usersFindByAge = await this.usersRepository.findAllByAge(Number(age));

    if (!usersFindByAge) {
      throw new Error('Not found users with this age');
    }

    const updatedUsers = usersFindByAge.map(user => {
      delete user.password;

      return user;
    });

    return updatedUsers;
  }
}

module.exports = ListUsersByAgeService;
