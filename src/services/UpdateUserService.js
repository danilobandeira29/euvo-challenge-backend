const { hashSync } = require('bcrypt');

class UpdateUserService {
  constructor(userRepository) {
    this.usersRepository = userRepository;
  }

  async execute({ name, email, cpf, password, id, age }) {
    const passwordCrypted = hashSync(password, 10);

    const findUserWithSameEmail = await this.usersRepository.findByEmail(email);

    if (findUserWithSameEmail.length !== 0) {
      throw new Error('This E-mail is already used');
    }

    await this.usersRepository.update({
      name,
      email,
      cpf,
      age,
      password: passwordCrypted,
      id: Number(id),
    });
  }
}

module.exports = UpdateUserService;
