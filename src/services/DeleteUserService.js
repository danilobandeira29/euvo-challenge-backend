class DeleteUserService {
  constructor(usersRepository) {
    this.usersRepository = usersRepository;
  }

  async execute(id) {
    await this.usersRepository.deleteById(id);
  }
}

module.exports = DeleteUserService;
