class ListUserByIdService {
  constructor(usersRepository) {
    this.usersRepository = usersRepository;
  }

  async execute(id) {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new Error('User not found');
    }

    delete user.password;

    return user;
  }
}

module.exports = ListUserByIdService;
