const Connection = require('../database/connections');

class UsersRepository {
  constructor() {
    this.users = new Connection();
  }

  async create({ name, email, cpf, password, age }) {
    const userId = await this.users.knexRepository
      .insert({
        name,
        email,
        cpf,
        password,
        age,
      })
      .table('user');
    return userId;
  }

  async findAll() {
    const allUsers = await this.users.knexRepository.select('*').from('user');

    const deletePassword = allUsers.map(user => {
      delete user.password;
      return { ...user };
    });

    return deletePassword;
  }

  async findAllByAge(age) {
    const findUsersByAge = await this.users.knexRepository
      .table('user')
      .where('age', '=', age);

    return findUsersByAge;
  }

  async findById(id) {
    const findUserById = await this.users.knexRepository
      .table('user')
      .select('*')
      .where('id', id);

    return findUserById[0] || undefined;
  }

  async update({ id, name, email, password, cpf, age }) {
    await this.users.knexRepository
      .table('user')
      .where('id', id)
      .update({ name, email, password, cpf, age });
  }

  async deleteById(id) {
    await this.users.knexRepository.table('user').where('id', id).del();
  }

  async findByEmail(email) {
    const findUserByEmail = await this.users.knexRepository
      .select('email')
      .from('user')
      .where('email', email);

    return findUserByEmail;
  }
}

module.exports = UsersRepository;
