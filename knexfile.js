const config = {
  client: 'mysql',
  connection: {
    host: '127.0.0.1',
    user: 'root',
    password: '',
    filename: './src/database/db.sql',
    database: 'db_euvo',
    insecureAuth: true,
  },
  migrations: {
    directory: './src/database/migrations',
  },
  useNullAsDefaults: true,
};

module.exports = config;
