<p align="center">
  <img src="https://ik.imagekit.io/xfddek6eqk/euvo_logo_zk8WXhH-E.png" alt="Euvo logo"/>
</p>

# Sumário
- [Sobre](#sobre)
- [Desafio](#desafio)
- [Tecnologias](#tecnologias)
- [Formatadores de Código](#formatadores-de-código)
- [Banco de dados](#banco-de-dados)
- [Baixar o Projeto](#baixar-o-projeto)
- [Rotas da API](#rotas-da-api)
- [Futuras implementações](#futuras-implementações)


## Sobre
API REST com as funcionalidades de uma CRUD(Create, Read, Update, Delete) para Usuários.

## Desafio
- [x] Proposto pela Euvo, onde eu tive que fazer uma api rest para a criação, listagem e exclusão de usuário.

## Tecnologias
- NodeJS
- API REST
- Micro-router
- KnexJS
- Mysql

## Formatadores de Código
- ESLint
- Prettier
- editorconfig

## Banco de Dados
### Programas Necessários
- DBeaver ou MySQL Workbench ou qualquer outra CLI de sua preferência para banco MySQL.

### Criando o Banco
1. Com a CLI aberta(no meu caso DBeaver), criar uma conexão com o banco MySQL, como mostra na imagem abaixo:
<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/database-gostack/backend-gostack-1_oCQkdt9IJ.png">
</p>

2. Selecionar o banco, nesse caso o MySQL e clicar em next:
<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/dbeaver_banco_mysql_mBPovFWnaH.png
">
</p>

3. Configurar de acordo com a image abaixo:
<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/dbeaver_banco_configurar_mysql_KBD1z7l7h.png">
</p>

4. E assim será criado a conexão com o banco mysql. Agora você deve abrir a conexão clicando duas vezes e assim será possível visualizar como mostra na imagem abaixo:
<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/dbeaver_37enw6K0K4.png">
</p>

5. Clique com o botão direito sobre *Databases*, em seguida *Create New Database*, e **insira o nome do banco como db_euvo** no campo *Database name* em seguida clique em *OK*:
<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/create_db_TVN82jjHdF.png">
</p>

### Configuração do Knex com o Banco
Você pode configurar a conexão com o banco de acordo com sua necessidade, basta alterar o arquivo *knexfile.js* localizado na pasta raíz do projeto.

<p align="center">
<img src="https://ik.imagekit.io/xfddek6eqk/config_db__PER5DRlQp.png">
</p>


## Baixar o Projeto
- Abra seu terminal para executar as seguintes linhas de comando:
```bash
  ## clonar repositório via SSH
  $ git clone git@gitlab.com:danilobandeira29/euvo-challenge-backend.git

  ## entrar no diretório
  $ cd euvo-challenge-backend

  ## instalar as dependências do projeto(ou você pode executar 'yarn')
  $ npm install

  ## executar migrations(ou você pode executar 'yarn db:setup')
  $ npm run db:setup

  ## inicializando o servidor(ou você pode executar 'yarn dev')
  ## ou pode utilizar yarn start, no caso do npm será npm run start
  $ npm run dev

```

## Rotas da API
- **GET** **`/user?age=18`** - Listagem de **todos os usuários com idade igual a 18 anos**, informando a age(idade) via query params.
- **GET** **`/user/:id`** - Listagem de um **usuário**, informando o **id** do mesmo no parâmetro de rota(route params).
- **POST** **`/user`** - Criar um novo **usuário**, informando *email*, *name*, *password*, *cpf* e *age* no corpo da requisição.
- **PUT** **`/:id`** - Atualizar todos os dados do usuário(*email*, *name*, *password*, *cpf* e *age*).
- **DELETE** **`/:id`** - Deletar um **usuário**, informando o **id** do mesmo no parâmetro de rota(route params).

## Futuras implementações
- [ ] Remover o knex e utilizar algum ORM(Sequelize, TypeORM)
- [ ] DDD
- [ ] TDD
- [ ] SOLID
- [ ] Serializar objetos antes de envia-los ao front-end

**Developed by/Desenvolvido por** 👻 <a href="https://www.linkedin.com/in/danilo-bandeira-4411851a4/">**Danilo Bandeira**</a>
